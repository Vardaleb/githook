use githook::git::github::Event;
use githook::{git::config::GitHookConfig, script};
use rocket::State;

fn get_params(
    config: &State<GitHookConfig>,
    event: Event,
    data: Option<String>,
) -> (String, Option<Event>, Option<String>) {
    let mut script = config.dir();
    let mut param_event: Option<Event> = None;
    if let Some(handler) = config.handler() {
        script.push_str(handler);
        param_event = Some(event);
    } else {
        script.push_str(&event.as_script());
    }

    (script, param_event, data)
}

fn internal_handle_event(config: &State<GitHookConfig>, event: Event, data: Option<String>) {
    let (script, param_event, data) = get_params(config, event, data);
    script::execute(&script, param_event.as_ref(), data);
}

#[post("/", data = "<data>")]
pub fn handle_event(config: &State<GitHookConfig>, event: Event, data: Option<String>) {
    internal_handle_event(config, event, data);
}

#[cfg(test)]
mod tests {
    use githook::git::{config::ConfigBuilder, github::Event};
    use rocket::State;

    use super::get_params;

    #[test]
    fn test_get_params1() {
        let binding = ConfigBuilder::new().build();
        let config = State::from(&binding);
        let event = Event::new(String::from("event"));
        let data = Some(String::from("{}"));

        let (s, e, d) = get_params(config, event, data);

        assert_eq!(s, "./event.sh");
        assert!(e.is_none());
        assert!(d.is_some_and(|s| s.eq("{}")));
    }

    #[test]
    fn test_get_params2() {
        let binding = ConfigBuilder::new().with_handler("handler.sh").build();
        let config = State::from(&binding);
        let event = Event::new(String::from("event"));
        let data = Some(String::from("{}"));

        let (s, e, d) = get_params(config, event, data);

        assert_eq!(s, "./handler.sh");
        assert!(e.is_some_and(|event| event.name().eq("event")));
        assert!(d.is_some_and(|s| s.eq("{}")));
    }

    #[test]
    fn test_get_params3() {
        let binding = ConfigBuilder::new()
            .with_dir("/tmp/scripts")
            .with_handler("handler.sh")
            .build();
        let config = State::from(&binding);
        let event = Event::new(String::from("event"));
        let data = Some(String::from("{}"));

        let (s, e, d) = get_params(config, event, data);

        assert_eq!(s, "/tmp/scripts/handler.sh");
        assert!(e.is_some_and(|event| event.name().eq("event")));
        assert!(d.is_some_and(|s| s.eq("{}")));
    }

    #[test]
    fn test_get_params4() {
        let binding = ConfigBuilder::new().with_dir("/tmp/scripts").build();
        let config = State::from(&binding);
        let event = Event::new(String::from("event"));
        let data = Some(String::from("{}"));

        let (s, e, d) = get_params(config, event, data);

        assert_eq!(s, "/tmp/scripts/event.sh");
        assert!(e.is_none());
        assert!(d.is_some_and(|s| s.eq("{}")));
    }
}
