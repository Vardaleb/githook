use githook::git::config::GitHookConfig;
use rocket::{fairing::AdHoc, Build, Rocket};

#[macro_use]
extern crate rocket;

#[cfg(test)]
mod tests;

pub mod handler;

fn mount_base(rocket: &Rocket<Build>) -> String {
    if let Ok(config) = rocket.figment().extract::<GitHookConfig>() {
        if let Some(mount_base) = config.mount_base() {
            return String::from(mount_base);
        }
    }
    "/".to_string()
}

#[launch]
fn rocket() -> _ {
    let rocket = rocket::build();
    let base_path = mount_base(&rocket);
    rocket
        .mount(base_path, routes![handler::handle_event,])
        .attach(AdHoc::config::<GitHookConfig>())
}
