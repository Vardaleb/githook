use std::{
    fs,
    path::{Path, PathBuf},
    process::Command,
};

use crate::git::github::Event;

/// Executes a `script` with the parameters `event` and `data`
///
/// `script` must be a full path to a shell script. If the script cannot be canonicalized,
/// an error is loged in rocket:error log
///
/// Both paramteters (`event` and `data`) are optional.
///
/// # Examples
///
/// This code executes the script `/tmp/demo.sh` with one parameter (a JSON structure)
/// ```
/// fn main() {
///     use githook::git::github::Event;
///     use githook::script;
///
///     let script = String::from("/tmp/demo.sh");
///     let event: Option<&Event> = None;
///     let data = Some(String::from("{ \"field\":\"value\" }"));
///     script::execute(&script, event, data);
/// }
/// ```
///
pub fn execute(script: &str, event: Option<&Event>, data: Option<String>) {
    let path = fs::canonicalize(Path::new(script));
    match path {
        Ok(path) => {
            rocket::info!("Executing script '{}'...", path.display());
            let mut command = create_command(&path, event, data);

            match command.spawn() {
                Ok(_) => rocket::info!("Executed script {}", path.display()),
                Err(err) => {
                    rocket::error!("Error executing script '{}': {}", path.display(), err)
                }
            }
        }
        Err(err) => rocket::error!("Script '{}' not found: {}", script, err),
    }
}

/// Creates a [`Command`] with the program from `path` and the optional parameters `event` and `data`
/// 
/// See [`execute`]
///
fn create_command(path: &PathBuf, event: Option<&Event>, data: Option<String>) -> Command {
    let mut command = Command::new(path);
    if let Some(event) = event {
        rocket::info!(" with event '{}'...", event.name());
        command.arg(event.name());
    }

    if let Some(data) = data {
        command.arg(data);
    }
    command
}

#[cfg(test)]
mod tests {
    use crate::git::github::Event;

    use super::create_command;
    use std::{ffi::OsStr, path::PathBuf};

    fn get_path() -> PathBuf {
        let mut path = PathBuf::new();
        path.push("demo.sh");
        path
    }

    #[test]
    fn test_create_command_no_event_no_data() {
        let path = get_path();
        let command = create_command(&path, None, None);
        assert_eq!(command.get_program(), "demo.sh");
        assert_eq!(command.get_args().count(), 0);
    }

    #[test]
    fn test_create_command_with_event_no_data() {
        let path = get_path();
        let command = create_command(&path, Some(&Event::new(String::from("event_name"))), None);
        assert_eq!(command.get_program(), "demo.sh");
        assert_eq!(command.get_args().count(), 1);
        assert_eq!(command.get_args().next(), Some(OsStr::new("event_name")));
    }

    #[test]
    fn test_create_command_no_event_with_data() {
        let path = get_path();
        let command = create_command(&path, None, Some("data".to_owned()));
        assert_eq!(command.get_program(), "demo.sh");
        assert_eq!(command.get_args().count(), 1);
        let mut args = command.get_args();
        assert_eq!(args.next(), Some(OsStr::new("data")));
    }

    #[test]
    fn test_create_command_with_event_with_data() {
        let path = get_path();
        let command = create_command(
            &path,
            Some(&Event::new(String::from("event_name"))),
            Some("data".to_owned()),
        );
        assert_eq!(command.get_program(), "demo.sh");
        assert_eq!(command.get_args().count(), 2);
        let mut args = command.get_args();
        assert_eq!(args.next(), Some(OsStr::new("event_name")));
        assert_eq!(args.next(), Some(OsStr::new("data")));
    }
}
