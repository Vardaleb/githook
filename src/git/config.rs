use serde::Deserialize;

/// Configuration variables for the githook
///
/// * `hook_mount_base`: Optional base path for the mount. Defaults to `"/"`
/// * `hook_dir`: Directory where the scripts to execute on events can be found. Defaults to `"./"`
/// * `hook_handler`:
///     A script to handle all events. If not configured scripts with the name of the
///     event will be called with the JSON payload as parameter $1.
///     The name of the event is parameter `$1`, the JSON payload is parameter `$2`
///
#[derive(Debug, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct GitHookConfig {
    hook_mount_base: Option<String>,
    hook_dir: Option<String>,
    hook_handler: Option<String>,
}

impl GitHookConfig {
    fn new() -> Self {
        Self {
            hook_mount_base: None,
            hook_dir: None,
            hook_handler: None,
        }
    }

    fn set_mount_base(&mut self, value: &str) {
        self.hook_mount_base = Some(value.to_string());
    }

    fn set_dir(&mut self, dir: &str) {
        self.hook_dir = Some(dir.to_string());
    }

    fn set_handler(&mut self, handler: &str) {
        self.hook_handler = Some(handler.to_string());
    }

    /// Returns the value of `hook_mount_base`
    pub fn mount_base(&self) -> Option<&String> {
        self.hook_mount_base.as_ref()
    }

    /// Returns the full path of `hook_dir`. The result always ends with a `/`
    pub fn dir(&self) -> String {
        let mut script = String::new();

        script.push_str(self.hook_dir.clone().unwrap_or(String::from(".")).as_str());
        if !script.ends_with("/") {
            script.push_str("/");
        }
        script
    }

    /// Returns the value of `hook_handler`
    pub fn handler(&self) -> Option<&String> {
        self.hook_handler.as_ref()
    }
}

///
/// Builder for the GitHookConfig (mainly for testing purposes)
///
/// # Example:
///
/// ```
/// use githook::git::config::ConfigBuilder;
/// fn example() {
///     let expected = "/configured/directory";
///     let config = ConfigBuilder::new().with_dir(&expected).build();
///     println!("{}", config.dir());
/// }
/// ```
pub struct ConfigBuilder {
    config: GitHookConfig,
}

impl ConfigBuilder {
    /// Creates a [ConfigBuilder]
    pub fn new() -> Self {
        ConfigBuilder {
            config: GitHookConfig::new(),
        }
    }

    /// Adds a `hook_mount_base` to the builder
    pub fn with_mount_base(&mut self, value: &str) -> &mut ConfigBuilder {
        self.config.set_mount_base(value);
        self
    }

    /// Adds a `hook_dir` to the builder
    pub fn with_dir(&mut self, dir: &str) -> &mut ConfigBuilder {
        self.config.set_dir(dir);
        self
    }

    /// Adds a `hook_handler` to the builder
    pub fn with_handler(&mut self, handler: &str) -> &mut ConfigBuilder {
        self.config.set_handler(handler);
        self
    }

    /// Builds the [GitHookConfig]
    pub fn build(&self) -> GitHookConfig {
        self.config.clone()
    }
}
