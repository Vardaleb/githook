use rocket::http::{HeaderMap, Status};
use rocket::outcome::Outcome;
use rocket::request::{self, FromRequest, Request};

const HEADER_EVENT: &str = "X-GitHub-Event";

fn get_header<'a>(headers: &'a HeaderMap, key: &str) -> Option<&'a str> {
    headers.get(key).next()
}

#[derive(Debug)]
pub enum Error {
    MissingHeader,
}

#[derive(Debug)]
pub struct Event {
    name: String,
}

impl Event {
    /// Creates a new `Event` with the given `name`
    pub fn new(name: String) -> Self {
        Self { name }
    }

    fn from_request<'r>(request: &'r Request<'_>) -> Option<Event> {
        let request_headers = request.headers();
        if let Some(event) = get_header(request_headers, HEADER_EVENT) {
            return Some(Event::new(event.to_string()));
        }
        None
    }

    /// Returns the name of this event
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Returns the name of the event as shell script by appending `.sh`
    pub fn as_script(&self) -> String {
        format!("{}.sh", self.name)
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Event {
    type Error = Error;

    async fn from_request(request: &'r Request<'_>) -> request::Outcome<Self, Self::Error> {
        if let Some(event) = Event::from_request(request) {
            return Outcome::Success(event);
        }

        Outcome::Forward(Status::Continue)
    }
}
