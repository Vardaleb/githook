use githook::git::config::ConfigBuilder;

#[test]
fn test_get_script_dir() {
    let expected = "/this/is/the/configured/directory/";
    let config = ConfigBuilder::new().with_dir(&expected).build();

    let script_dir = config.dir();
    assert_eq!(expected, script_dir);
}

#[test]
fn test_get_script_dir_no_slash_at_end() {
    let expected = "/configured/directory";
    let config = ConfigBuilder::new().with_dir(&expected).build();

    let script_dir = config.dir();
    assert!(script_dir.ends_with("/"));
}

#[test]
fn test_get_script_dir_empty() {
    let expected = "./";
    let config = ConfigBuilder::new().build();

    let script_dir = config.dir();
    assert_eq!(expected, script_dir);
}
