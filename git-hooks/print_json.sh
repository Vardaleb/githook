#!/bin/bash
echo "==========================="
echo "Generic Git WebHook handler"
echo "==========================="
EVENT=$1
JSON=$2
echo $JSON > $EVENT.last.json
case $EVENT in
    push)
        read commits repo user < <(echo $(echo $JSON | 
            jq -r '.total_commits, .repository.name, .sender.email'))    
        echo "push: $user performed $commits commits on repository '$repo'"
        ;;
    issues)
        read action id repo user < <(echo $(echo $JSON | 
            jq -r '.action, .issue.id, .repository.name, .sender.email'))    
        echo "issues: $user performed '$action' for issue #$id on repository '$repo'"
        ;;
    issue_comment)
        read action id comment_id repo user < <(echo $(echo $JSON | 
            jq -r '.action, .issue.id, .comment.id, .repository.name, .sender.email'))    
        echo "issue comment: $user performed '$action' on the comment #$comment_id for issue #$id on repository '$repo'"
        ;;
    *)
        echo $JSON | jq 
        echo $JSON > $EVENT.unknown.json
        echo "Unknown event: $EVENT"
        ;;
esac
echo "------------------------"
