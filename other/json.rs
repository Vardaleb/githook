use rocket::serde::json::{Error, Json};
use serde::Serialize;
use std::{fs::File, io::Write, path::Path};

pub fn extract_json<T>(data: &Json<T>) -> Option<String>
where
    T: Serialize + Clone,
{
    let event = data.clone().into_inner();
    let json = match serde_json::to_string(&event) {
        Ok(json) => json,
        Err(err) => {
            rocket::error!("Could not serialize the date to JSON: {err}");
            return None;
        }
    };
    Some(json)
}

fn write_json<T>(event: &str, data: &Json<T>)
where
    T: Serialize + Clone,
{
    let json = match extract_json(data) {
        Some(value) => value,
        None => return,
    };

    write_string(&format!("{event}.json"), &json);
}

pub fn write_string(file_name: &str, content: &String) {
    let path = Path::new(&file_name);
    let mut file = match File::create(&path) {
        Ok(file) => file,
        Err(err) => {
            rocket::error!("Could not write to file {file_name}: {err}");
            return;
        }
    };

    if let Err(err) = file.write_all(content.as_bytes()) {
        rocket::error!("Could not write file {}: {err}", path.display());
    }
}

pub fn write_unknown<T>(event: &String, data: &Result<&Json<T>, Error>)
where
    T: Serialize + Clone,
{
    // write JSON data to a file
    match &data {
        Ok(data) => write_json(event, &data),
        Err(err) => match err {
            Error::Io(err) => rocket::error!("Error parsing JSON: {}", err),
            Error::Parse(json, err) => {
                rocket::error!("Error parsing JSON: {}", err);
                write_string(&format!("{}.unknown.json", event), &json.to_string());
            }
        },
    }
}
