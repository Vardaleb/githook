use rocket::serde::Deserialize;
use rocket::serde::Serialize;
use serde_json::Value;

use super::common;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Event {
    pub action: String,
    pub number: i64,
    pub changes: Changes,
    #[serde(rename = "pull_request")]
    pub pull_request: PullRequest,
    #[serde(rename = "requested_reviewer")]
    pub requested_reviewer: Option<Value>,
    pub repository: common::Repository,
    pub sender: common::User,
    #[serde(rename = "commit_id")]
    pub commit_id: String,
    pub review: Option<Value>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Changes {
    pub title: Title,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Title {
    pub from: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PullRequest {
    pub id: i64,
    pub url: String,
    pub number: i64,
    pub user: common::User,
    pub title: String,
    pub body: String,
    pub labels: Vec<Value>,
    pub milestone: Option<Value>,
    pub assignee: Option<Value>,
    pub assignees: Option<Value>,
    #[serde(rename = "requested_reviewers")]
    pub requested_reviewers: Option<Value>,
    pub state: String,
    #[serde(rename = "is_locked")]
    pub is_locked: bool,
    pub comments: i64,
    #[serde(rename = "html_url")]
    pub html_url: String,
    #[serde(rename = "diff_url")]
    pub diff_url: String,
    #[serde(rename = "patch_url")]
    pub patch_url: String,
    pub mergeable: bool,
    pub merged: bool,
    #[serde(rename = "merged_at")]
    pub merged_at: Option<String>,
    #[serde(rename = "merge_commit_sha")]
    pub merge_commit_sha: Option<String>,
    #[serde(rename = "merged_by")]
    pub merged_by: Option<common::User>,
    #[serde(rename = "allow_maintainer_edit")]
    pub allow_maintainer_edit: bool,
    pub base: common::Revision,
    pub head: common::Revision,
    #[serde(rename = "merge_base")]
    pub merge_base: String,
    #[serde(rename = "due_date")]
    pub due_date: Option<Value>,
    #[serde(rename = "created_at")]
    pub created_at: String,
    #[serde(rename = "updated_at")]
    pub updated_at: String,
    #[serde(rename = "closed_at")]
    pub closed_at: Option<String>,
    #[serde(rename = "pin_order")]
    pub pin_order: i64,
}
