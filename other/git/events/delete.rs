use rocket::serde::Deserialize;
use rocket::serde::Serialize;

use super::common;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Event {
    #[serde(rename = "ref")]
    pub ref_field: String,
    #[serde(rename = "ref_type")]
    pub ref_type: String,
    #[serde(rename = "pusher_type")]
    pub pusher_type: String,
    pub repository: common::Repository,
    pub sender: common::User,
}
