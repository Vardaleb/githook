use rocket::serde::Deserialize;
use rocket::serde::Serialize;
use serde_json::Value;

use super::common;
use super::common::Repository;

///
/// Generated using https://transform.tools/json-to-rust-serde
///
#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Event {
    #[serde(rename = "ref")]
    pub ref_field: String,
    pub before: String,
    pub after: String,
    #[serde(rename = "compare_url")]
    pub compare_url: String,
    pub commits: Vec<Commit>,
    #[serde(rename = "total_commits")]
    pub total_commits: i64,
    #[serde(rename = "head_commit")]
    pub head_commit: Commit,
    pub repository: Repository,
    pub pusher: common::User,
    pub sender: common::User,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Commit {
    pub id: String,
    pub message: String,
    pub url: String,
    pub author: Author,
    pub committer: Author,
    pub verification: Option<Value>,
    pub timestamp: String,
    pub added: Option<Value>,
    pub removed: Option<Value>,
    pub modified: Option<Value>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Author {
    pub name: String,
    pub email: String,
    pub username: String,
}

