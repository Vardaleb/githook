use rocket::serde::Deserialize;
use rocket::serde::Serialize;
use serde_json::Value;

use super::common;

///
/// Generated using https://transform.tools/json-to-rust-serde
///
#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Event {
    pub action: String,
    pub issue: Issue,
    pub comment: Option<Comment>,
    pub repository: common::Repository,
    pub sender: common::User,
    #[serde(rename = "is_pull")]
    pub is_pull: Option<bool>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Issue {
    pub id: i64,
    pub url: String,
    #[serde(rename = "html_url")]
    pub html_url: String,
    pub number: i64,
    pub user: common::User,
    #[serde(rename = "original_author")]
    pub original_author: String,
    #[serde(rename = "original_author_id")]
    pub original_author_id: i64,
    pub title: String,
    pub body: String,
    #[serde(rename = "ref")]
    pub ref_field: String,
    pub assets: Vec<Value>,
    pub labels: Vec<Value>,
    pub milestone: Option<Value>,
    pub assignee: Option<Value>,
    pub assignees: Option<Value>,
    pub state: String,
    #[serde(rename = "is_locked")]
    pub is_locked: bool,
    pub comments: i64,
    #[serde(rename = "created_at")]
    pub created_at: String,
    #[serde(rename = "updated_at")]
    pub updated_at: String,
    #[serde(rename = "closed_at")]
    pub closed_at: Option<Value>,
    #[serde(rename = "due_date")]
    pub due_date: Option<Value>,
    #[serde(rename = "pull_request")]
    pub pull_request: Option<Value>,
    pub repository: SimpleRepository,
    #[serde(rename = "pin_order")]
    pub pin_order: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SimpleRepository {
    pub id: i64,
    pub name: String,
    pub owner: String,
    #[serde(rename = "full_name")]
    pub full_name: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Comment {
    pub id: i64,
    #[serde(rename = "html_url")]
    pub html_url: String,
    #[serde(rename = "pull_request_url")]
    pub pull_request_url: String,
    #[serde(rename = "issue_url")]
    pub issue_url: String,
    pub user: common::User,
    #[serde(rename = "original_author")]
    pub original_author: String,
    #[serde(rename = "original_author_id")]
    pub original_author_id: i64,
    pub body: String,
    pub assets: Vec<Value>,
    #[serde(rename = "created_at")]
    pub created_at: String,
    #[serde(rename = "updated_at")]
    pub updated_at: String,
}
