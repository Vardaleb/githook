#!/bin/bash
HEADERS=$1.headers
CONTENT=$1.json
METHOD=POST
PORT=4273
TARGET=http://localhost:$PORT/$2

# Debug headers
function debug()
{
    TARGET=https://httpbin.org/headers
    METHOD=GET
    echo "${headers[@]}"
    for header in "${headers[@]}"; do
    # printf -v header_string "%s%s" "$header_string" "$header"
        echo $header
    done
}

function createHeaders() 
{
    headers=()
    while IFS=':' read -r key value; do
    # Skip empty lines or lines starting with # (comments)
    [[ -z "$key" || "$key" =~ ^# ]] && continue
    headers+=("$key:$value")
    done < $HEADERS
}

function call()
{
    curl \
        -i -X \
        "${headers[@]/#/-H}" \
        --request $METHOD \
        --data-binary @$CONTENT \
        $TARGET
}

createHeaders
call