# githook

A web hook to monitor git events for
- github
- gitea 
- (gitlab)
# Description
The `githook` is a simple rocket based web server, that calls shell scripts, when a webhook POST is received. There are two ways to execute shell scripts:
1. Event scripts
2. Generic handler
## Event scripts
Each Git Webhook event has a header for the event type (`X-GitHub-Event`). The value of this header is taken as a script name: `<event>.sh`. The script is taken from the configured `hook_dir`.
For example, the `X-GitHub-Event` for events of type `push` will execute the shell script `<hook_dir>/push.sh`. The payload of the event is a JSON that will be passed as argument to the event script.
### Example (`<hook_dir>/push.sh`)
```bash
#!/bin/bash
JSON=$1
read commits repo user < <(echo $(echo $JSON | 
    jq -r '.total_commits, .repository.name, .sender.email'))    
echo "push: $user performed $commits commits on repository '$repo'"
```
## Generic handler
When there is a `hook_handler` configured, this script is then executed with the event as first parameter, and the JSON payload as the second parameter. The handler script must also be placed in the `hook_dir` directory.
### Example (`<hook_dir>/<hook_handler>`)
```bash
#!/bin/bash
EVENT=$1
JSON=$2
case $EVENT in
    push)
        read commits repo user < <(echo $(echo $JSON | 
            jq -r '.total_commits, .repository.name, .sender.email'))    
        echo "push: $user performed $commits commits on repository '$repo'"
        ;;
    issues)
        read action id repo user < <(echo $(echo $JSON | 
            jq -r '.action, .issue.id, .repository.name, .sender.email'))    
        echo "issues: $user performed '$action' for issue #$id on repository '$repo'"
        ;;
    issue_comment)
        read action id comment_id repo user < <(echo $(echo $JSON | 
            jq -r '.action, .issue.id, .comment.id, .repository.name, .sender.email'))    
        echo "issue comment: $user performed '$action' on the comment #$comment_id for issue #$id on repository '$repo'"
        ;;
    *)
        echo $JSON | jq 
        echo $JSON > $EVENT.unknown.json
        echo "Unknown event: $EVENT"
        ;;
esac
```

# Configuration
Configuration is read from the rocket configuration.
## Options
| Option | Environment variable | Default | Description |
|--------|----------------------|---------|-------------|
| hook_mount_base | ROCKET_HOOK_MOUNT_BASE | "/" | The mount base for the rocket web server |
| hook_dir | ROCKET_HOOK_DIR | "./" | The directory for the shell scripts |
| hook_handler | ROCKET_HOOK_HANDLER | None | If the handler is configured, the configured script is called instead of event specific scripts. When it is not configured, each event is calling a script with the name `<event>.sh`. |


# Testing
## Simulate a post
* Using the script `post.sh`
    * first parameter is the name of a header/content combination. 
    * Example: `./post.sh data/issue` will POST the headers from file `./data/issue.headers` with the body of `./data/issue.json`
## Setup a tunnel
To test the webhook locally with a git provider online, a tunnel can be used:
* See [localhost.run](http://localhost.run/docs/forever-free)
