# Builder
FROM rust:latest AS builder

RUN rustup target add x86_64-unknown-linux-musl
RUN apt update && apt install -y musl-tools musl-dev
RUN update-ca-certificates

# Create app user
ENV USER=githook
ENV UID=1000

RUN adduser \
    --disabled-password \
    --gecos "" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /githook
COPY ./ .
RUN cargo build --target x86_64-unknown-linux-musl --release

# Image
FROM alpine

RUN apk update \
    && apk add bash jq

# Import from builder
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
WORKDIR /githook
RUN chown -R githook:githook /githook
RUN chmod 755 /githook

# Copy the built binary
COPY --from=builder /githook/target/x86_64-unknown-linux-musl/release/githook ./
COPY ./LICENSE ./
COPY ./Rocket.toml ./
COPY ./git-hooks/print_json.sh ./hooks/

USER githook:githook

EXPOSE 4273
CMD ["./githook"]
